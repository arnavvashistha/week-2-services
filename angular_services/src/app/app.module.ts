import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule,MatToolbarModule,MatCardModule} from '@angular/material';
import {MatGridListModule} from '@angular/material/grid-list';

import {MatListModule} from '@angular/material/list';

import { NgModule } from '@angular/core';
import 'hammerjs';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { DishdetailComponent } from './dishdetail/dishdetail.component';
import{DishService} from "./services/dish.service";
import { FlexLayoutModule } from "@angular/flex-layout";


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DishdetailComponent
  ],
  imports: [
    BrowserModule,BrowserAnimationsModule,MatButtonModule,MatCheckboxModule,MatToolbarModule,MatListModule,MatGridListModule,MatCardModule,FlexLayoutModule
  ],
  providers: [DishService],
  bootstrap: [AppComponent]
})
export class AppModule { }
